ACCOUNT PROFILE / USER DELETE BRIDGE
------------------------------------

Functionality
-------------
This module enables Account Profile and User Delete modules to work together.


Requirements
------------
- Account Profile
- User Delete


Installation
------------
- Extract the files and copy "account_profile_user_delete_bridge" folder to your 
  "sites/all/modules/" directory.
- Enable the module at "admin/build/modules".
- Go to "admin/user/user_delete" and set the default action when deleting. 
  The options available prior to installation of this module include leaving 
  the content on site or assigning it to the anonymous user, but this should obviously 
  not be done with the account profile information. Therefore, even if you already
  set this action before installing this module, you need to set it again.


Authors
-------
This module was developed by Covenant Web Design (www.covenantdesign.com).
Development sponsored by Teacher Cooperative (www.teachercooperative.com)
